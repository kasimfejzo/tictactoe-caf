#include <caf/all.hpp>

struct play {};
struct startGame {};
struct pX {};
struct pO {};

caf::behavior player(caf::event_based_actor* self) {
  
  return caf::behavior {
    [](play, std::string n) {
      std::cout << "Player " << n << ", enter row: ";
      int r;
      std::cin >> r;
      std::cout << "Player " << n << ", enter column: ";
      int c;
      std::cin >> c;
      
      return std::pair<int, int>(r, c);
    } 
  };
}

struct scoreHolderState {
  caf::actor playerX;
  caf::actor playerO;

  std::vector<std::vector<std::string>> v = {
    {" ", " ", " "},
    {" ", " ", " "},
    {" ", " ", " "}
  };
};

const void print(const std::vector<std::vector<std::string>>& v) {
  std::cout << "#############" << std::endl;
  std::cout << "# " << v[0][0] << " # " << v[0][1] << " # " << v[0][2] << " #" << std::endl;
  std::cout << "#############" << std::endl;
  std::cout << "# " << v[1][0] << " # " << v[1][1] << " # " << v[1][2] << " #" << std::endl;
  std::cout << "#############" << std::endl;
  std::cout << "# " << v[2][0] << " # " << v[2][1] << " # " << v[2][2] << " #" << std::endl;
  std::cout << "#############" << std::endl;
}

bool checkForWinner(const std::vector<std::vector<std::string>>& v, std::string p) {
  if(v[0][0] == p && v[0][1] == p && v[0][2] == p) return true;
  if(v[1][0] == p && v[1][1] == p && v[1][2] == p) return true;
  if(v[2][0] == p && v[2][1] == p && v[2][2] == p) return true;
  
  if(v[0][0] == p && v[1][1] == p && v[2][2] == p) return true;
  if(v[0][2] == p && v[1][1] == p && v[2][0] == p) return true;
  
  return false;
}

bool checkForEnd(const std::vector<std::vector<std::string>>& v) {
  
  for(size_t i = 0; i < v.size(); ++i) 
    for(size_t j = 0; j < v[i].size(); ++ j) 
      if(v[i][j] == " ") return false;

  return true;
}

void playerInputHandler(caf::stateful_actor<scoreHolderState>* self, const std::string& player, const std::pair<int, int>& p) {
  self -> state.v[p.first][p.second] = player;
  print(self -> state.v);
  if(checkForWinner(self -> state.v, player)) {
    std::cout << "Player " + player + " wins this game." << std::endl;
    self -> quit();
  }
  else if(checkForEnd(self -> state.v)) {
    std::cout << "Draw" << std::endl;
    self -> quit();
  }
}

caf::behavior scoreHolder(caf::stateful_actor<scoreHolderState>* self) {
  self -> state.playerX = self -> home_system().spawn(player);
  self -> state.playerO = self -> home_system().spawn(player);

  return caf::behavior {
    [self](startGame) {
      self -> send(self, pX{}); 
    },

    [self](pX) {
      self -> request(self -> state.playerX, caf::infinite, play{}, "X").await(
        [self](std::pair<int, int> p) {
          playerInputHandler(self, "X", p);
          self -> send(self, pO{});
        }
      );
    },
    
    [self](pO) {
      self -> request(self -> state.playerO, caf::infinite, play{}, "O").await(
        [self](std::pair<int, int> p) {
          playerInputHandler(self, "O", p);
          self -> send(self, pX{});
        }
      );
    }

  };

}

int main() {
  caf::actor_system_config cfg;
  caf::actor_system sys{cfg};
  
  caf::actor scoreH = sys.spawn(scoreHolder);
  caf::anon_send(scoreH, startGame{});
  
  return 0;
}
